import Vue from 'vue'

Vue.filter('limit', function (value) {
  return value.slice(0, 250) + '...'
})

Vue.filter('ilkharf', function (value) {
  return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('reverse', function (value) {
  return value.slice().reverse()
})
